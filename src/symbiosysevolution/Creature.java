/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symbiosysevolution;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 *
 * @author Matus
 */
public class Creature implements Comparable<Creature>
{
    private ArrayList<BigInteger> codingSequence;
    private BigInteger regulatorySequence;
    private int energy;
    private int symbiosisCounter;
    private int parasiteCounter;
    private int noReactionCounter;
    private int damageCounter;
    private int mutationCounter;
    private int ancestorCounter;
    
    public Creature(int geneCount, int geneSize, Random rnd)
    {
        if(rnd == null)
        {
            throw new IllegalArgumentException("Random cannot be null!");
        }
        if(geneCount <= 0 || geneSize <= 0)
        {
            throw new IllegalArgumentException("Invalid parameters for gene sequence.");
        }  
        resetStats();
        mutationCounter = 0;
        ancestorCounter = 0;
        this.codingSequence = new ArrayList<>();
        for(int i = 0; i < geneCount; ++i)
        {
            this.codingSequence.add(new BigInteger(geneSize, rnd));
        }
        this.regulatorySequence = new BigInteger(geneCount, rnd);                
    }
    
    public Creature(List<BigInteger> codingSequence, BigInteger regulatorySequence)
    {
        resetStats();
        mutationCounter = 0;
        ancestorCounter = 0;
        this.codingSequence = new ArrayList<>();
        for(BigInteger seq : codingSequence)
        {
            this.codingSequence.add(seq);
        }
        this.regulatorySequence = regulatorySequence;     
    }
    
    public final void resetStats()
    {
        energy = 0;          
        symbiosisCounter = 0;
        parasiteCounter = 0;
        noReactionCounter = 0;
        damageCounter = 0;        
    }

    public ArrayList<BigInteger> getCodingSequence() {
        return codingSequence;
    }

    public BigInteger getRegulatorySequence() {
        return regulatorySequence;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
    
    public synchronized void addToEnergy(int i)
    {
        energy += i;
    } 

    public int getSymbiosisCounter() {
        return symbiosisCounter;
    }

    public void setSymbiosisCounter(int symbiosisCounter) {
        this.symbiosisCounter = symbiosisCounter;
    }
    
    public synchronized void addToSymbiosisCounter(int i)
    {
        symbiosisCounter += i;
    }

    public int getParasiteCounter() {
        return parasiteCounter;
    }

    public void setParasiteCounter(int parasiteCounter) {
        this.parasiteCounter = parasiteCounter;
    }
    
    public synchronized void addToParasiteCounter(int i)
    {
        parasiteCounter += i;
    }

    public int getNoReactionCounter() {
        return noReactionCounter;
    }

    public void setNoReactionCounter(int noReactionCounter) {
        this.noReactionCounter = noReactionCounter;
    }

    public synchronized void addToNoReactionCounter(int i)
    {
        noReactionCounter += i;
    }
    
    public int getDamageCounter() {
        return damageCounter;
    }

    public void setDamageCounter(int damageCounter) {
        this.damageCounter = damageCounter;
    }
    
    public synchronized void addToDamageCounter(int i)
    {
        damageCounter += i;
    }

    public int getMutationCounter() {
        return mutationCounter;
    }

    public void setMutationCounter(int mutationCounter) {
        this.mutationCounter = mutationCounter;
    }

    public int getAncestorCounter() {
        return ancestorCounter;
    }

    public void setAncestorCounter(int ancestorCounter) {
        this.ancestorCounter = ancestorCounter;
    }
    
    @Override
    public int compareTo(Creature other) {
        return this.getEnergy() - other.getEnergy();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.codingSequence);
        hash = 29 * hash + Objects.hashCode(this.regulatorySequence);
        hash = 29 * hash + this.energy;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Creature other = (Creature) obj;
        if (!Objects.equals(this.codingSequence, other.codingSequence)) {
            return false;
        }
        if (!Objects.equals(this.regulatorySequence, other.regulatorySequence)) {
            return false;
        }
        return this.energy == other.energy;
    }
    
    public static String getBigIntegerString(BigInteger seq, int numOfBits)
    {
        String result = "";
        for(int i = 0; i < numOfBits; ++i)
        {
            if(seq.testBit(i))
            {
                result = "1" + result;
            }else{
                result = "0" + result;
            }
        }
        return result;
    }
}
