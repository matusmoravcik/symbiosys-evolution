/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symbiosysevolution;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matus
 */
public class Evolver {
    private volatile boolean running;
    
    private int geneCount;
    private int geneSize;
    private int populationSize;
    private double proliferateTreshold;
    private int generations;
    private int interactions;
    private double mutationRate;
    
    private double symbiosisTreshold;
    private int symbiosisReward;
    private double parasiteTreshold;
    private int parasiteReward;
    private int parasiteDamage;
    
    private ArrayList<Creature> population;
    
    private volatile int progress;
    
    public final static Random RND = new Random();
    
    private int outputFrequency;
    private ConcurrentLinkedQueue<String> output;
    private boolean arffMode;
    
    private final static int CORES = Runtime.getRuntime().availableProcessors();
    
    public Evolver()
    {
        population = new ArrayList<>();
        
        geneCount = 1;
        geneSize = 1;
        populationSize = 0;
        proliferateTreshold = 0.0;
        generations = 0;
        interactions = 0;
        mutationRate = 0.0;
        
        symbiosisTreshold = 0.75;
        symbiosisReward = 1; 
        parasiteTreshold = 1;
        parasiteReward = 4;
        parasiteDamage = 1;
        
        outputFrequency = 1;
        output = new ConcurrentLinkedQueue();
    }

    public int getProgress() {
        return progress;
    }

    public int getGeneCount() {
        return geneCount;
    }

    public void setGeneCount(int geneCount) {
        this.geneCount = geneCount;
    }

    public int getGeneSize() {
        return geneSize;
    }

    public void setGeneSize(int geneSize) {
        this.geneSize = geneSize;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int populationSize) {
        this.populationSize = populationSize;
    }

    public double getProliferateTreshold() {
        return proliferateTreshold;
    }

    public void setProliferateTreshold(double proliferateTreshold) {
        this.proliferateTreshold = proliferateTreshold;
    }

    public int getGenerations() {
        return generations;
    }

    public void setGenerations(int generations) {
        this.generations = generations;
    }

    public int getInteractions() {
        return interactions;
    }

    public void setInteractions(int interactions) {
        this.interactions = interactions;
    }

    public double getMutationRate() {
        return mutationRate;
    }

    public void setMutationRate(double mutationRate) {
        this.mutationRate = mutationRate;
    }

    public double getSymbiosisTreshold() {
        return symbiosisTreshold;
    }

    public void setSymbiosisTreshold(double symbiosisTreshold) {
        this.symbiosisTreshold = symbiosisTreshold;
    }

    public int getSymbiosisReward() {
        return symbiosisReward;
    }

    public void setSymbiosisReward(int symbiosisReward) {
        this.symbiosisReward = symbiosisReward;
    }

    public double getParasiteTreshold() {
        return parasiteTreshold;
    }

    public void setParasiteTreshold(double parasiteTreshold) {
        this.parasiteTreshold = parasiteTreshold;
    }

    public int getParasiteReward() {
        return parasiteReward;
    }

    public void setParasiteReward(int parasiteReward) {
        this.parasiteReward = parasiteReward;
    }

    public int getParasiteDamage() {
        return parasiteDamage;
    }

    public void setParasiteDamage(int parasiteDamage) {
        this.parasiteDamage = parasiteDamage;
    }

    public int getOutputFrequency() {
        return outputFrequency;
    }
    
    public void setOutputFrequency(int outputFrequency) {
        this.outputFrequency = outputFrequency;
    }

    public ConcurrentLinkedQueue<String> getOutput() {
        return output;
    }   

    public boolean isArffMode() {
        return arffMode;
    }

    public void setArffMode(boolean arffMode) {
        this.arffMode = arffMode;
    }
    
    public boolean isRunning() {
        return running;
    }

    public void stop()
    {
        this.running = false;
    }
    
    public void run()
    {
        running = true;
        output.clear();
        new Thread(new Runnable(){
            @Override
            public void run()
            {
                population = new ArrayList<>();
                for(int i = 0; i < populationSize; ++i)
                {
                    population.add(new Creature(geneCount, geneSize, RND));
                }
                
                int treshold = (int) (proliferateTreshold * population.size());                
                
                if(arffMode)
                {
                    output.add("@RELATION EVOLUTION\n\n");   
                    for(int i = 0; i < geneSize * geneCount + geneCount; ++i)
                    {
                        output.add("@ATTRIBUTE dna" + i + " {0,1}\n");
                    }
                    output.add("@ATTRIBUTE generation NUMERIC\n@ATTRIBUTE energy NUMERIC\n@ATTRIBUTE mutualism NUMERIC\n@ATTRIBUTE parasitism NUMERIC\n@ATTRIBUTE noreaction NUMERIC\n@ATTRIBUTE damageTaken NUMERIC\n@ATTRIBUTE mutationCount NUMERIC\n@ATTRIBUTE ancestorsCount NUMERIC\n\n@DATA\n");
                }
                else
                {
                    output.add("Generation 0:\n");
                    for(Creature c : population)
                    {
                        output.add(getDNAString(c) + "\n");
                    }
                    output.add("\n");
                }  
                
                progress = 0;
                while(running && progress < generations)
                {
                    ++progress;     
                    
                    //interactions
                    final CountDownLatch latch = new CountDownLatch(CORES);
                    for(int i = 0; i < CORES; ++i)
                    {
                        new Thread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                for(int cycle = 0; cycle < interactions / CORES; ++cycle)
                                {
                                    for(Creature c : population)
                                    {
                                        interact(c, population.get(RND.nextInt(population.size())));
                                    }
                                }
                                latch.countDown();
                            }
                        }).start();  
                    }
                    
                    for(int cycle = 0; cycle < interactions % CORES; ++cycle)
                    {
                        for(Creature c : population)
                        {
                            interact(c, population.get(RND.nextInt(population.size())));
                        }
                    }
                    
                    try {
                        latch.await();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Evolver.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    //log output
                    if(progress % outputFrequency == 0)
                    {
                        if(arffMode)
                        {
                            for(Creature c : population)
                            {
                                output.add(getAsCSV(c)
                                    + "," + progress
                                    + "," + c.getEnergy()
                                    + "," + c.getSymbiosisCounter()
                                    + "," + c.getParasiteCounter()
                                    + "," + c.getNoReactionCounter()
                                    + "," + c.getDamageCounter()
                                    + "," + c.getMutationCounter()
                                    + "," + c.getAncestorCounter()
                                    + "\n");
                            }
                        }
                        else
                        {
                            output.add("Generation " + progress + ":\n");
                            for(Creature c : population)
                            {
                                output.add(getDNAString(c) + "\n");
                            }
                            output.add("\n");
                        }
                    }

                    //select
                    Collections.sort(population); 
                    for(int j = treshold; j < population.size(); ++j)
                    {
                        if(population.get(j).getEnergy() <= 0)
                        {
                            continue;
                        }

                        ArrayList<BigInteger> newCodingSequence = new ArrayList<>(population.get(j).getCodingSequence());
                        BigInteger newReglatorySequence = population.get(j).getRegulatorySequence();
                        
                        //mutate
                        boolean mutate = RND.nextFloat() < mutationRate;
                        /*if(progress * 2 < generations)
                        {
                            mutate = true;
                        }*/
                        if(progress >= generations - 1)
                        {
                            mutate = false;
                        }
                        if(mutate)
                        { 
                            if(RND.nextBoolean())
                            {
                                int gene = RND.nextInt(newCodingSequence.size());
                                newCodingSequence.set(gene, newCodingSequence.get(gene).flipBit(RND.nextInt(geneSize)));
                            }else{
                                newReglatorySequence = newReglatorySequence.flipBit(RND.nextInt(newCodingSequence.size()));
                            }
                        }
                        
                        //reproduce
                        Creature offspring = new Creature(newCodingSequence, newReglatorySequence);                        
                        if(mutate)
                        {
                            offspring.setMutationCounter(population.get(j).getMutationCounter() + 1);
                        }else{
                            offspring.setMutationCounter(population.get(j).getMutationCounter());
                        }
                        offspring.setAncestorCounter(population.get(j).getAncestorCounter() + 1);
                        population.set(j - treshold, offspring);
                    }
                    
                    //reset stats
                    for(Creature c : population)
                    {
                        c.resetStats();
                    } 
                }                
                running = false;
            }
        }).start();        
    }  
    
    private void interact(Creature c1, Creature c2) //parameter order sensitive!
    {
        for(int seq = 0; seq < geneCount; ++seq)
        {
            c1.addToNoReactionCounter(1);
            if(c1.getRegulatorySequence().testBit(seq))
            {
                if((c1.getCodingSequence().get(seq).xor(c2.getCodingSequence().get(seq))).bitCount() >= geneSize * symbiosisTreshold)
                {
                    c1.addToEnergy(symbiosisReward);
                    c1.addToSymbiosisCounter(1);
                    c1.addToNoReactionCounter(-1);                    
                }
            }else{
                if(c2.getRegulatorySequence().testBit(seq) && (c1.getCodingSequence().get(seq).xor(c2.getCodingSequence().get(seq))).bitCount() >= geneSize * parasiteTreshold)
                {
                    c1.addToEnergy(parasiteReward);                    
                    c1.addToParasiteCounter(1);
                    c1.addToNoReactionCounter(-1);

                    c2.addToEnergy(-parasiteDamage);
                    c2.addToDamageCounter(1); 
                }
            }
        } 
    }
    
    public String getDNAString(Creature c)
    {
        String result = "";
        for(BigInteger seq : c.getCodingSequence())
        {
            result += Creature.getBigIntegerString(seq, geneSize) + "_";
        }
        return result + "  " + Creature.getBigIntegerString(c.getRegulatorySequence(), geneCount);
    }
    
    public String getWholeDNAString(Creature c)
    {
        String result = "";
        for(BigInteger seq : c.getCodingSequence())
        {
            result += Creature.getBigIntegerString(seq, geneSize);
        }
        return result + Creature.getBigIntegerString(c.getRegulatorySequence(), geneCount);
    }
    
    public String getAsCSV(Creature c)
    {
        String dna = getWholeDNAString(c);
        String result = "" + dna.charAt(0);
        for(int i = 1; i < dna.length(); ++i)
        {
            result = result + "," + dna.charAt(i);
        }
        return result;
    }
}
